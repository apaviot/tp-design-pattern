Exercice 1

1. 

	Les tests unitaires servent à tester une classe de façon totalement isolée du reste du code.
	Les tests d'intégration vont quand à eux permettre de tester le code dans sa globalité, en vérifiant que les composants
	communiquent bien entre eux et tout cela avec des données de test proches des données de production.

2.

	Selon moi la méthodologie TDD est adaptée lorsqu'on travaille à plusieurs. Une bonne idée serait qu'un développeur
	prépare des tests puis qu'un autre développeur écrive le code. Ce dernier pourrait alors se concentrer uniquement
	sur le fait de devoir valider les test.

3.

	Le mieux que l'on puisse faire pour vérifier que les tests unitaires JUnit sont suffisants par rapport au code du
	programme est de lancer un test de couverture. Néanmoins, même si ce test révèle que le code est entièrement couvert,
	cela ne signifie pas que le code est dépourvu de bug. Le développeur peut avoir oublier d'écrire un test pour vérifier
	l'appel d'une méthode avec une valeur particulière (on test généralement avec 0 pour les entiers et null pour les objets)
	et cela ne se verra pas lors test de couverture.