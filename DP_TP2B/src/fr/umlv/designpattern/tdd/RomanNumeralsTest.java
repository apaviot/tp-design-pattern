package fr.umlv.designpattern.tdd;

import static org.junit.Assert.*;

import org.junit.Test;

public class RomanNumeralsTest {

	@Test
	public void testToInteger1() {
		assertEquals(1, RomanNumerals.toInteger("I"));
		assertEquals(5, RomanNumerals.toInteger("V"));
		assertEquals(10, RomanNumerals.toInteger("X"));
		assertEquals(50, RomanNumerals.toInteger("L"));
		assertEquals(100, RomanNumerals.toInteger("C"));
	}

	@Test
	public void testToInteger2() {
		assertEquals(6, RomanNumerals.toInteger("VI"));
		assertEquals(15, RomanNumerals.toInteger("XV"));
		assertEquals(27, RomanNumerals.toInteger("XXVII"));
		assertEquals(40, RomanNumerals.toInteger("XL"));
		assertEquals(60, RomanNumerals.toInteger("LX"));
		assertEquals(90, RomanNumerals.toInteger("XC"));
		assertEquals(150, RomanNumerals.toInteger("CL"));
		assertEquals(166, RomanNumerals.toInteger("CLXVI"));
		assertEquals(278, RomanNumerals.toInteger("CCLXXVIII"));
		assertEquals(777, RomanNumerals.toInteger("CCCCCCCLXXVII"));
	}

	@Test(expected = NullPointerException.class)
	public void testToInteger3() {
		RomanNumerals.toInteger(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testToInteger4() {
		RomanNumerals.toInteger("ABC");
	}

	@Test
	public void testToRomanNumerals1() {
		assertEquals("I", RomanNumerals.toRomanNumerals(1));
		assertEquals("V", RomanNumerals.toRomanNumerals(5));
		assertEquals("X", RomanNumerals.toRomanNumerals(10));
		assertEquals("L", RomanNumerals.toRomanNumerals(50));
		assertEquals("C", RomanNumerals.toRomanNumerals(100));
	}

	@Test
	public void testToRomanNumerals2() {
		assertEquals("VI", RomanNumerals.toRomanNumerals(6));
		assertEquals("XV", RomanNumerals.toRomanNumerals(15));
		assertEquals("XXVII", RomanNumerals.toRomanNumerals(27));
		assertEquals("XL", RomanNumerals.toRomanNumerals(40));
		assertEquals("LX", RomanNumerals.toRomanNumerals(60));
		assertEquals("XC", RomanNumerals.toRomanNumerals(90));
		assertEquals("CL", RomanNumerals.toRomanNumerals(150));
		assertEquals("CLXVI", RomanNumerals.toRomanNumerals(166));
		assertEquals("CCLXXVIII", RomanNumerals.toRomanNumerals(278));
		assertEquals("CCCCCCCLXXVII", RomanNumerals.toRomanNumerals(777));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testToRomanNumerals3() {
		RomanNumerals.toRomanNumerals(0);
	}

	@Test
	public void testToIntegerPrefix() {
		assertEquals(4, RomanNumerals.toInteger("IV"));
		assertEquals(9, RomanNumerals.toInteger("IX"));
		assertEquals(40, RomanNumerals.toInteger("XL"));
		assertEquals(49, RomanNumerals.toInteger("XLIX"));
		assertEquals(90, RomanNumerals.toInteger("XC"));
	}

	@Test
	public void testToRomanNumeralsPrefix() {
		assertEquals("IV", RomanNumerals.toRomanNumerals(4));
		assertEquals("IX", RomanNumerals.toRomanNumerals(9));
		assertEquals("XL", RomanNumerals.toRomanNumerals(40));
		assertEquals("XLIX", RomanNumerals.toRomanNumerals(49));
		assertEquals("XC", RomanNumerals.toRomanNumerals(90));
	}
}