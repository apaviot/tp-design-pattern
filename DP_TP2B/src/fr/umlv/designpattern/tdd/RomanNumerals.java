package fr.umlv.designpattern.tdd;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RomanNumerals {

	private static final int[] VALUES = { 100, 90, 50, 40, 10, 9, 5, 4, 1 };
	private static final String[] DIGRAMS = { "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I" };

	public static int toInteger(String string) {
		int res = 0;
		Pattern pattern = Pattern.compile("^(C*)(XC)?(L?)(XL)?(X{0,3})(IX)?(V)?(IV)?(I{0,3})$");
		Matcher m = pattern.matcher(string);

		if (m.matches()) {
			for (int i = 1; i <= DIGRAMS.length; i++) {
				if (m.group(i) != null) {
					res += VALUES[i - 1] * (m.group(i).length() / DIGRAMS[i - 1].length());
				}
			}
		} else {
			throw new IllegalArgumentException("The given roman number is not valid: " + string);
		}
		return res;
	}

	public static String toRomanNumerals(int i) {
		if (i < 1) {
			throw new IllegalArgumentException("The value should be greater than 1: " + i);
		}
		StringBuilder sb = new StringBuilder();
		for (int index = 0; index < VALUES.length; index++) {
			while (i >= VALUES[index]) {
				sb.append(DIGRAMS[index]);
				i -= VALUES[index];
			}
		}
		return sb.toString();
	}

}
