package fr.umlv.equilibrium4;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Iterator;

public class Game {

	static void play(Player p1, Player p2) {
		/*
		 * if (p1.cards.size() != p2.cards.size()) { throw new IllegalStateException("not same number of cards"); } if
		 * (p1.cards.stream().mapToInt(Card::getDamage).sum() != p2.cards.stream().mapToInt(Card::getDamage).sum()) {
		 * throw new IllegalStateException("no equilibrium"); }
		 */

		Iterator<Monster> it1 = p1.getMonsters();
		Iterator<Monster> it2 = p2.getMonsters();
		while (it1.hasNext() && it2.hasNext()) {
			p2.hit(it1.next().getDamages());
			p1.hit(it2.next().getDamages());

			if (p1.isDead() && p2.isDead()) {
				System.out.println("no winner");
				return;
			}
			if (p1.isDead()) {
				System.out.println(p2 + " wins");
				return;
			}
			if (p2.isDead()) {
				System.out.println(p1 + " wins");
				return;
			}
		}
		System.out.println("draw");
	}

	public static void main(String[] args) throws IOException {
		Player p1 = Player.createPlayer("bob", Paths.get("td3/bob-deck.txt"));
		Player p2 = Player.createPlayer("alice", Paths.get("td3/alice-deck.txt"));
		play(p1, p2);
	}
}