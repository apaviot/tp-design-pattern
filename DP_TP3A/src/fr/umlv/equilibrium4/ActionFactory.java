package fr.umlv.equilibrium4;

import java.util.ArrayList;
import java.util.Objects;

public class ActionFactory {
	ArrayList<Action> actions = new ArrayList<>();

	private ActionFactory() {
	}

	public static ActionFactory createInstance() {
		return new ActionFactory();
	}

	public Action create(String name, String[] tokens) {
		Objects.requireNonNull(name);
		Objects.requireNonNull(tokens);
		switch (name) {
		case "monster" :
			return (p1, p2) -> p2.hit(Integer.parseInt(tokens[1]));
		case "lightning":
			return (p1, p2) -> {
				int damage = Integer.parseInt(tokens[1]);
				p1.hit(damage/2);
				p2.hit(damage);
			};
		default :
			throw new IllegalStateException("Unknow action: " + name);
		}
	}

}
