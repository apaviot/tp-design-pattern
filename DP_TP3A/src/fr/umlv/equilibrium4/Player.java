package fr.umlv.equilibrium4;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import fr.umlv.parsing.Parser;

public class Player {

	private final String name;
	private final List<Action> monsters;
	private int health;

	private Player(String name, List<Action> monsters) {
		this.name = name;
		this.health = 5;
		this.monsters = monsters;
	}
	
	public static Player createPlayer(String name, Path path, ActionFactory factory) throws IOException {
		ArrayList<Action> monsters = new ArrayList<>();
		Parser.parse(path,  l -> {
			String[] tokens = l.split("");
			monsters.add(factory.create(tokens[0], tokens));
		});
		return new Player(Objects.requireNonNull(name), monsters);
	}
	
	public boolean isDead() {
		return health <= 0;
	}
	
	@Override
	public String toString() {
		return name + ": " + health;
	}
	
	public void hit(int damage) {
		health -= damage;
	}
	
	public Iterator<Monster> getMonsters() {
		return Collections.unmodifiableList(monsters).iterator();
	}
}