package fr.umlv.equilibrium4;


public class Lightning implements Action {
	
	private final int damage;
	
	public Lightning(int damage) {
		this.damage = damage;
	}

	@Override
	public void perform(Player player1, Player player2) {
		player1.hit(damage / 2);
		player2.hit(damage);
	}

}
