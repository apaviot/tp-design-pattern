package fr.umlv.equilibrium4;


public interface Action {
	void perform(Player player1, Player player2);
}
