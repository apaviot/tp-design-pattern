package fr.umlv.equilibrium4;

public class Monster implements Action {

	private final int damage;

	public Monster(int damage) {
		this.damage = requireValidBounds(damage, 1, 5);
	}

	private static int requireValidBounds(int value, int min, int max) {
		if (value < min || value > max) {
			throw new IllegalArgumentException("" + value);
		}
		return value;
	}
	
	public int getDamages() {
		return damage;
	}

	@Override
	public void perform(Player player1, Player player2) {
		player2.hit(damage);
	}
}