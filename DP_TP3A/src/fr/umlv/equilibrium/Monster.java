package fr.umlv.equilibrium;

public class Monster {

	public final int damage;

	public Monster(int damage) {
		this.damage = requireValidBounds(damage, 1, 5);
	}

	private static int requireValidBounds(int value, int min, int max) {
		if (value < min || value > max) {
			throw new IllegalArgumentException("" + value);
		}
		return value;
	}
}