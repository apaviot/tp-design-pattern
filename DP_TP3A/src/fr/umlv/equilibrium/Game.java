package fr.umlv.equilibrium;

import java.io.IOException;
import java.util.Iterator;

public class Game {

	static void play(Player p1, Player p2) {
		/*
		 * if (p1.cards.size() != p2.cards.size()) { throw new IllegalStateException("not same number of cards"); } if
		 * (p1.cards.stream().mapToInt(Card::getDamage).sum() != p2.cards.stream().mapToInt(Card::getDamage).sum()) {
		 * throw new IllegalStateException("no equilibrium"); }
		 */

		Iterator<Monster> it1 = p1.monsters.iterator();
		Iterator<Monster> it2 = p2.monsters.iterator();
		while (it1.hasNext() && it2.hasNext()) {
			p2.health -= it1.next().damage;
			p1.health -= it2.next().damage;

			if (p1.health <= 0 && p2.health <= 0) {
				System.out.println("no winner");
				return;
			}
			if (p1.health <= 0) {
				System.out.println(p2.name + " wins");
				return;
			}
			if (p2.health <= 0) {
				System.out.println(p1.name + " wins");
				return;
			}
		}
		System.out.println("draw");
	}

	public static void main(String[] args) throws IOException {
		Player p1 = new Player("bob", "td3/bob-deck.txt");
		Player p2 = new Player("alice", "td3/alice-deck.txt");
		play(p1, p2);
	}
}