package fr.umlv.equilibrium;

import static java.util.Objects.requireNonNull;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.stream.Stream;

public class Player {

	public final String name;
	public final ArrayList<Monster> monsters = new ArrayList<>();
	public int health;

	public Player(String name, String path) throws IOException {
		this.name = requireNonNull(name);
		this.health = 5;
		try (Stream<String> lines = Files.lines(Paths.get(path))) {
			lines.forEach(line -> {
				monsters.add(new Monster(Integer.parseInt(line.split(" ")[1])));
			});
		}
	}
}