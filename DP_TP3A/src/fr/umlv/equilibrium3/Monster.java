package fr.umlv.equilibrium3;

public class Monster {

	private final int damage;

	public Monster(int damage) {
		this.damage = requireValidBounds(damage, 1, 5);
	}

	private static int requireValidBounds(int value, int min, int max) {
		if (value < min || value > max) {
			throw new IllegalArgumentException("" + value);
		}
		return value;
	}
	
	public int getDamages() {
		return damage;
	}
}