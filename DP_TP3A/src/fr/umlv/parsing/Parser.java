package fr.umlv.parsing;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.stream.Stream;

public class Parser {
	
	public static void parse(Path path, Consumer<String> lineConsumer) throws IOException {
		Objects.requireNonNull(path);
		Objects.requireNonNull(lineConsumer);
		try (Stream<String> lines = Files.lines(path)) {
			lines.forEach(lineConsumer::accept);
		}
	}

}
