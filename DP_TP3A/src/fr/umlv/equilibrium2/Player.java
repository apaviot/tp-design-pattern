package fr.umlv.equilibrium2;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import fr.umlv.parsing.Parser;

public class Player {

	private final String name;
	private final List<Monster> monsters;
	private int health;

	private Player(String name, List<Monster> monsters) {
		this.name = name;
		this.health = 5;
		this.monsters = monsters;
	}
	
	public static Player createPlayer(String name, Path path) throws IOException {
		ArrayList<Monster> monsters = new ArrayList<>();
		Parser.parse(path,  l -> monsters.add(new Monster(Integer.parseInt(l.split("")[1]))));
		return new Player(Objects.requireNonNull(name), monsters);
	}
}