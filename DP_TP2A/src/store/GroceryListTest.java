package store;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;


class GroceryListTest {

	@Test
	void testCreateGroceryListWithGoodName() {
		new GroceryList("list");
	}
	
	@Test
	void testCreateGroceryListWithNameNull() {
		assertThrows(NullPointerException.class, () -> {
			new GroceryList(null);
		});
	}
	
	@Test
	void testAddOrderWithFruitNull() {
		assertThrows(NullPointerException.class, () -> {
			GroceryList g = new GroceryList("list");
			g.addOrder(null, 1);
		});
	}
	
	@Test
	void testAddOrderWithCountNull() {
		assertThrows(IllegalArgumentException.class, () -> {
			GroceryList g = new GroceryList("list");
			g.addOrder(new Fruit("Pineapple", 7), 0);
		});
	}
	
	@Test
	void testAddOrderWithCountNegative() {
		assertThrows(IllegalArgumentException.class, () -> {
			GroceryList g = new GroceryList("list");
			g.addOrder(new Fruit("Pineapple", 7), -1);
		});
	}
	
	@Test
	void testGetFruitQuantity() {
		GroceryList g = new GroceryList("list");
		Fruit mango = new Fruit("Mango", 9);
		g.addOrder(mango, 2);
		assertEquals(2, g.getQuantityOfFruit(mango));
	}
	
	@Test
	void testGetFruitQuantityWithNotExistingFruit() {
		GroceryList g = new GroceryList("list");
		Fruit mango = new Fruit("Mango", 9);
		assertEquals(0, g.getQuantityOfFruit(mango));
	}
	
	@Test
	void testGetFruitQuantityWithFruitNull() {
		GroceryList g = new GroceryList("list");
		assertThrows(NullPointerException.class, () -> {
			assertEquals(0, g.getQuantityOfFruit(null));
		});
	}
	
	@Test
	void testPrice() {
		GroceryList g = new GroceryList("list");
		g.addOrder(new Fruit("Grenada", 14), 2);
		assertEquals(28, g.price());
	}
	
	@Test
	void testToStringEmptyGroceryList() {
		GroceryList g = new GroceryList("Kooki");
		Fruit grenada = new Fruit("Grenada", 14);
		g.addOrder(grenada, 2);
		assertEquals("list for Kooki\n" + grenada.toString() + " * 2\nprice: 28", g.toString());
	}

}
