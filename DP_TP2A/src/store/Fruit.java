package store;

import java.util.Objects;

public class Fruit {

	private final String name;
	private final long price;

	public Fruit(String name, long price) {
		this.name = Objects.requireNonNull(name);
		if (price <= 0) {
			throw new IllegalArgumentException("The price shouln't be negative: " + price);
		}
		this.price = price;
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Fruit)) {
			return false;
		}
		Fruit fruit = (Fruit) o;
		return name.equals(fruit.name) && price == fruit.price;
	}

	@Override
	public int hashCode() {
		return name.hashCode() & (int) price;
	}

	public String getName() {
		return name;
	}

	public long getPrice() {
		return price;
	}

	@Override
	public String toString() {
		return name + " (" + price + ')';
	}
}