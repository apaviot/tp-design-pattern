package store;

import static org.junit.Assert.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;


class FruitTest {
	
	@Test
	void testGetName() {
		Fruit fruit = new Fruit("apple", 2);
		assertEquals("apple", fruit.getName());
	}

	@Test
	void testGetPrice() {
		Fruit fruit = new Fruit("apple", 2);
		assertEquals(2, fruit.getPrice());
	}
	
	@Test
	void testEquals() {
		Fruit f1 = new Fruit("apple", 2);
		Fruit f2 = new Fruit("apple", 2);
		assertTrue(f1.equals(f2));
	}
	
	@Test
	void testEqualsSameObject() {
		Fruit f1 = new Fruit("apple", 2);
		assertTrue(f1.equals(f1));
	}
	
	@Test
	void testEqualsWithDifferentName() {
		Fruit f1 = new Fruit("apple", 2);
		Fruit f2 = new Fruit("apples", 2);
		assertFalse(f1.equals(f2));
	}
	
	@Test
	void testEqualsWithDifferentPrice() {
		Fruit f1 = new Fruit("apple", 2);
		Fruit f2 = new Fruit("apple", 3);
		assertNotEquals(f1, f2);
	}
	
	@Test
	void testEqualsWithDifferentNameAndPrice() {
		Fruit f1 = new Fruit("apple", 2);
		Fruit f2 = new Fruit("apples", 3);
		assertNotEquals(f1, f2);
	}
	
	@Test
	void testEqualsWithObjectOfDifferentClass() {
		Fruit f1 = new Fruit("apple", 2);
		assertNotEquals(f1, new Object());
	}
	
	@Test
	void testHashcodeWithEqualsObjects() {
		Fruit f1 = new Fruit("apple", 2);
		Fruit f2 = new Fruit("apple", 2);
		assertEquals(f1.hashCode(), f2.hashCode());
	}
	
	@Test
	void testCreateFruitWithNameNull() {
		assertThrows(NullPointerException.class, () -> {
			new Fruit(null, 1);
		});
	}
	
	@Test
	void createFruitWithNegativePrice() {
		assertThrows(IllegalArgumentException.class, () -> {
			new Fruit("Banana", -1);
		});
	}
	
	@Test
	void testToString() {
		Fruit fruit = new Fruit("Pineapple", 777);
		assertEquals("Pineapple (777)", fruit.toString());
	}

}
