package fr.umlv.vehicle;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
	
	List<Truck> createConvoy(Path path) throws IOException {
		try (Stream<String> lines = Files.lines(path)) {
			return lines.map(l -> {
				String[] tokens = l.split("");
				if (tokens.length < 3) {
					throw new IllegalStateException("No such elements to create truck: " + l);
				}
				return new Truck(Integer.parseInt(tokens[1]), tokens[2]);
			}).collect(Collectors.toList());
		}
	}
	
	int computeTax(List<Truck> trucks) {
		return trucks.stream().reduce(0, (tot,  t) -> tot + t.tax(), (tot1, tot2) -> tot1 + tot2);
	}

}
