package fr.umlv.vehicle;

import java.util.Objects;

public class Truck {
	private final int maxWeight;
	private final String color;
	
	public Truck(int maxWeight, String color) {
		this.maxWeight = checkMaxWeight(maxWeight);
		this.color = Objects.requireNonNull(color);
	}

	private int checkMaxWeight(int maxWeight) {
		if (maxWeight <= 0) {
			throw new IllegalArgumentException("maxWeight should be positive: " + maxWeight);
		}
		return maxWeight;
	}
	
	public int tax() {
		return maxWeight / 100;
	}
}
