package fr.umlv.vehicle2;

import java.util.Objects;

public class Moto implements Vehicle {
	
	private final String color;

	private Moto(String color) {
		this.color = color;
	}
	
	public static Moto createMoto(String color) {
		return new Moto(Objects.requireNonNull(color));
	}
	
	@Override
	public int tax() {
		return 0;
	}

}
