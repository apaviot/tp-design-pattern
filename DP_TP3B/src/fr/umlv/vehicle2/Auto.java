package fr.umlv.vehicle2;


public class Auto extends AbstractVehicle {
	private final int maxWeight;
	private final String color;
	private final int capacity;

	private Auto(int maxWeight, String color) {
		super(maxWeight, color);
	}
	
	private static int checkMaxWeight(int maxWeight) {
		if (maxWeight <= 0) {
			throw new IllegalArgumentException("maxWeight should be positive: " + maxWeight);
		}
		return maxWeight;
	}
	
	public static Auto createAuto(int maxWeight, String color) {
		return new Auto(maxWeight, color);
	}
	
	@Override
	public int tax() {
		return 155;
	}

}
