package fr.umlv.vehicle2;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
	
	List<Vehicle> createConvoy(Path path, VehicleFactory factory) throws IOException {
		try (Stream<String> lines = Files.lines(path)) {
			return lines.map(l -> {
				String[] tokens = l.split("");
				return factory.create(tokens[0], List.of(tokens));
			}).collect(Collectors.toList());
		}
	}
	
	int computeTax(List<Vehicle> convoy) {
		return convoy.stream().reduce(0, (tot,  t) -> tot + t.tax(), (tot1, tot2) -> tot1 + tot2);
	}
	
	public static void main(String[] args) {
		
	}

}
