package fr.umlv.vehicle2;

public class Truck implements Vehicle {
	private final int maxWeight;
	private final String color;
	
	private Truck(int maxWeight, String color) {
		this.maxWeight = checkMaxWeight(maxWeight);
		this.color = color;
	}
	
	private static int checkMaxWeight(int maxWeight) {
		if (maxWeight <= 0) {
			throw new IllegalArgumentException("maxWeight should be positive: " + maxWeight);
		}
		return maxWeight;
	}
	
	public static Truck createTruck(int maxWeight, String color) {
		return new Truck(maxWeight, color);
	}

	public int tax() {
		return maxWeight;
	}
}
