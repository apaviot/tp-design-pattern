package fr.umlv.vehicle2;

import java.util.ArrayList;
import java.util.Objects;

public class Convoy {
	private final ArrayList<Vehicle> vehicles;
	
	public Convoy() {
		vehicles = new ArrayList<>();
	}
	
	public void add(Vehicle vehicle) {
		vehicles.add(Objects.requireNonNull(vehicle));
	}
}
