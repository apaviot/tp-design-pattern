package fr.umlv.vehicle2;
import java.util.List;
import java.util.Objects;

public class VehicleFactory {
	public Vehicle create(String name, List<String> description) {
		Objects.requireNonNull(description);
		switch (Objects.requireNonNull(name)) {
		case "truck" :
			return Truck.createTruck(Integer.parseInt(description.get(0)), description.get(2));
		case "moto" :
			return Moto.createMoto(Integer.parseInt(description.get(0)), description.get(2));
		case "auto" :
			return Auto.createAuto(Integer.parseInt(description.get(0)), description.get(2));
		default:
			throw new IllegalStateException("Unknow vehicle type: " + name);
		}
	}
}
