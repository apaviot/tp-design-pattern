package fr.umlv.zengraph;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Optional;
import java.util.function.Consumer;

public class ShapeList implements Iterable<Shape> {
	private final ArrayList<Shape> shapes = new ArrayList<>();
	
	public ShapeList() {
	}
	
	public ShapeList(Collection<Shape> shapes) {
		this.shapes.addAll(shapes);
	}
	
	public boolean add(Shape shape) {
		return shapes.add(shape);
	}
	
	public boolean remove(Object o) {
		return shapes.remove(o);
	}
	
	public Optional<Shape> nearestShapeFromPoint(int x, int y) {
		return shapes.stream().reduce((s1, s2) -> {
			if (s1.distance(x, y) < s2.distance(x, y)) {
				return s1;
			}
			return s2;
		});
	}
	
	@Override
	public void forEach(Consumer<? super Shape> action) {
		shapes.forEach(action);
	}

	@Override
	public Iterator<Shape> iterator() {
		return shapes.iterator();
	}
	
	@Override
	public String toString() {
		return shapes.toString();
	}

}
