package fr.umlv.zengraph;

import java.awt.Graphics2D;

public interface Shape {
	void draw(Graphics2D graphics);

	int distance(int x, int y);
	
	private static int square(int a) {
		return a * a;
	}
	
	static int distanceBetweenPoints(int x1, int y1, int x2, int y2) {
		return square(x2 - x1) + square(y2 - y1);
	}
}
