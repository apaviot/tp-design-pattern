package fr.umlv.zengraph;

import java.awt.Color;
import java.awt.Graphics2D;
import java.io.IOException;
import java.util.Optional;

public class Main {

	private static void drawAll(Graphics2D graphics, ShapeList shapes) {
		graphics.setColor(Color.BLACK);
		shapes.forEach(l -> l.draw(graphics));
	}

	private static void loadShapeClasses() throws ClassNotFoundException {
		ClassLoader loader = Main.class.getClassLoader();
		Package p = loader.getDefinedPackage("fr.umlv.zengraph");
		System.out.println(p);
		Class.forName("fr.umlv.zengraph.Line", true, loader);
		Class.forName("fr.umlv.zengraph.Oval", true, loader);
		Class.forName("fr.umlv.zengraph.Rectangle", true, loader);
	}

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		loadShapeClasses();
		ShapeList shapes = Parser.parse(args[0], new ShapeFactory());
		CanvasArea area = new CanvasArea("area", 800, 600);
		area.clear(Color.WHITE);
		area.render(graphics -> drawAll(graphics, shapes));
		
		area.waitForMouseEvents((x, y) -> {
			Optional<Shape> nearest = shapes.nearestShapeFromPoint(x, y);
			nearest.ifPresent(s -> shapes.add(new ColoredShape(s, Color.ORANGE)));
			area.clear(Color.WHITE);
			area.render(graphics -> drawAll(graphics, shapes));
		});
			
	}

}
