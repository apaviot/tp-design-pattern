package fr.umlv.zengraph;

import java.awt.Color;

public class Test2 {
  private static void callback(CanvasArea area, int x, int y) {
    area.render(graphics -> {
      graphics.setColor(Color.BLACK);
      graphics.drawRect(x - 18, y - 18, 36, 36);
    });
  }
  
  public static void main(String[] args) {
    CanvasArea area = new CanvasArea("area", 800, 600);
    area.clear(Color.WHITE);
    area.waitForMouseEvents((x, y) -> callback(area, x, y));
  }
}