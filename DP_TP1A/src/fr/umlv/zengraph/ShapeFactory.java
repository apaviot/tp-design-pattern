package fr.umlv.zengraph;

import java.util.HashMap;
import java.util.Objects;
import java.util.function.Function;

public class ShapeFactory {
	private static final HashMap<String, Function<String[], Shape>> creatingFunctions = new HashMap<>();
	
	public static void register(String name, Function<String[], Shape> createFunction) {
		creatingFunctions.put(Objects.requireNonNull(name), Objects.requireNonNull(createFunction));
	}
	
	public Shape createShape(String name, String[] tokens) {
		return creatingFunctions.getOrDefault(name, i -> { throw new IllegalStateException("Unknown shape: " + name); })
			.apply(tokens);
	}

}
