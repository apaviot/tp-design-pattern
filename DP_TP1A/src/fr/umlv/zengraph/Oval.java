package fr.umlv.zengraph;

import java.awt.Graphics2D;

public class Oval implements Shape {
	
	static {
		ShapeFactory.register("oval", tokens -> {
			return Oval.createInstance(tokens);
		});
	}
	
	private final int x;
	private final int y;
	private final int width;
	private final int height;
	
	private Oval(int x, int y, int width, int height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}
	
	public static Oval createInstance(String[] tokens) {
		return new Oval(Integer.parseInt(tokens[1]),
			Integer.parseInt(tokens[2]),
			Integer.parseInt(tokens[3]),
			Integer.parseInt(tokens[4]));
	}

	@Override
	public void draw(Graphics2D graphics) {
		graphics.drawOval(x, y, width, height);
	}

	@Override
	public int distance(int x, int y) {
		return Shape.distanceBetweenPoints(this.x, this.y, x, y);
	}

}
