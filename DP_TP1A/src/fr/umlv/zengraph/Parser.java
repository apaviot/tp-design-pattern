package fr.umlv.zengraph;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Parser {

	static ShapeList parse(String name, ShapeFactory factory) throws IOException {
		Path path = Paths.get(name);
		try (Stream<String> lines = Files.lines(path)) {
			List<Shape> shapes = lines.map(line -> {
				String[] tokens = line.split(" ");
				return factory.createShape(tokens[0], tokens);
			}).collect(Collectors.toCollection(() -> new ArrayList<>()));
			return new ShapeList(shapes);
		}
	}

}
