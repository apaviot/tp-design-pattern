package fr.umlv.zengraph;

import java.awt.Color;
import java.awt.Graphics2D;

public class ColoredShape implements Shape {
	private final Shape shape;
	private final Color color;
	
	public ColoredShape(Shape shape, Color color) {
		super();
		this.shape = shape;
		this.color = color;
	}

	@Override
	public void draw(Graphics2D graphics) {
		graphics.setColor(color);
		shape.draw(graphics);
	}

	@Override
	public int distance(int x, int y) {
		return shape.distance(x, y);
	}
	
	

}
