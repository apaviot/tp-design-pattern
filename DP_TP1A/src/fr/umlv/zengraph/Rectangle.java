package fr.umlv.zengraph;

import java.awt.Graphics2D;

public class Rectangle implements Shape {
	
	static {
		ShapeFactory.register("rectangle", tokens -> {
			return new Rectangle(Integer.parseInt(tokens[1]),
				Integer.parseInt(tokens[2]),
				Integer.parseInt(tokens[3]),
				Integer.parseInt(tokens[4]));
		});
	}
	
	private final int x;
	private final int y;
	private final int width;
	private final int height;
	
	private Rectangle(int x, int y, int width, int height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}

	@Override
	public void draw(Graphics2D graphics) {
		graphics.drawRect(x, y, width, height);
	}

	@Override
	public int distance(int x, int y) {
		return Shape.distanceBetweenPoints(this.x, this.y, x, y);
	}
}
