package fr.umlv.zengraph;

import java.awt.Graphics2D;

public class Line implements Shape {
	
	static {
		ShapeFactory.register("line", tokens -> {
			return new Line(Integer.parseInt(tokens[1]),
				Integer.parseInt(tokens[2]),
				Integer.parseInt(tokens[3]),
				Integer.parseInt(tokens[4]));
		});
	}
	
	private final int x1;
	private final int y1;
	private final int x2;
	private final int y2;
	
	private Line(int x1, int y1, int x2, int y2) {
		super();
		this.x1 = x1;
		this.y1 = y1;
		this.x2 = x2;
		this.y2 = y2;
	}
	
	
	public void draw(Graphics2D graphics) {
		graphics.drawLine(x1, y1, x2, y2);
	}

	@Override
	public int distance(int x, int y) {
		int centerX = (x1 + x2) / 2;
		int centerY = (y1 + y2) / 2;
		return Shape.distanceBetweenPoints(centerX, centerY, x, y);
	}

	@Override
	public boolean equals(Object o) {
		if (! (o instanceof Line)) {
			return false;
		}
		Line l = (Line) o;
		return x1 == l.x1 && x2 == l.x2 && y1 == l.y1 && y2 == l.y2;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + x1;
		result = prime * result + x2;
		result = prime * result + y1;
		result = prime * result + y2;
		return result;
	}
}
