1.
	![Question 1](question1.jpg)

2.
	
	La classe Bonus possède la responsabilité de calculer le bonus de salaire d'un employé. La Classe Employee est responsable de la représentation d'un employé et du calcul de son salaire. La classe Payment est responsable du calcul total de la paye des employés.
	
	Un employé est composé d'un bonus (qui peut être null) et un objet de la classe Payment peut contenir plusieurs instances d'Employee.

3.
	
	> Les champs amount de Bonus et name de Employee sont déclarés finaux car ils sont très fortement liés à une instance. Si le montant du bonus venait à changer alors il est logique de créer une nouvelle instance de Bonus car il d'agit d'un nouveau bonus. De l'autre côté, un employé de doit pas changer de nom.

	> Il y a un test pour savoir si amount est négatif dans Bonus et deux tests pour savoir si salary est négatif dans Employee car on fait le test à chaque écriture dans les champs. Le champs amount est donc vérifié dans le constructeur de bonus, à savoir le seul endroit où une valeur peut lui être affectée tandis que salary est testé dans le constructeur et le setter.
	Pour éviter la petite duplication de code il est possible de créer une méthode statique dans une classe utilitaire.

	> La méthode Objects.requireNonNull permet de vérifier si la référence à un objet est nule ou non. Si tel est le cas, une NullPointerException est lancée. Cela permet de s'assurer lors de l'écriture dans un champs que celui-ci ne contiendra pas de référence null. Il n'y aura donc pas besoin de tester null lors de la lecture de ce champs dans la suite du programme (il y a plus de lecture que d'écriture).

	> Payment.getAllEmployees ne renvoie pas directement la référence sur employees afin de préserver l'encapsulation. De cette manière l'utilisateur ne peut pas modifier la liste qui lui est retournée. Le seul moyen d'ajouter un employé dans Payment est de passer par la méthode addEmployee de Payment.

	>

5.
	
	La méthode de Payment qui calcule la paye totale doit simplement parcourir tous les employés, leur "demander" de calculer leur salaire et d'effectuer la somme. Il n'est pas de la responsabilité de la classe Payement de savoir calculer le salaire d'un employé.

6.
	
	Les employés et les étudiants possèdent le même comportement à savoir le fait de toucher un salaire. Il est donc possible de créer une interface Payable qui possède une méthode qui renvoie le salaire touché.

	Du côté de la classe Payment il faut donc remplacer la liste d'Employee pour une liste de Payable et modifier le type des méthodes.
	
	
![Question 6](question6.jpg)

8.

9.
	
	L'architecture de classes établie précédemment ne permet pas de s'adapter simplement au nouveau scénario.
