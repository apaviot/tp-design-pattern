package test;

public class Bonus {
  final long amount;

  public Bonus(long amount) {
    this.amount = Utils.requirePositive(amount);
  }
}