package test;


public interface Payable {
	long payment();

	void setSalary(long salary);
	
	void setBonus(Bonus bonus);
}
