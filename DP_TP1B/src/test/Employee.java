package test;

import java.util.Objects;

public class Employee extends AbstractPayable {
	
	static {
		Payment.register(Employee.class, (s, l) -> new Employee(Objects.requireNonNull(s), Utils.requirePositive(l)));
	}

	private Employee(String name, long salary) {
		super(name, salary);
	}

	/*
	public static Employee create(String name, long salary, Payment payment) {
		Employee em = new Employee(name, salary);
		payment.addPayable(em);
		return em;
	}
	*/

	@Override
	public	long payment() {
		return bonus == null ? salary : salary + bonus.amount;
	}
}