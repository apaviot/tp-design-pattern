package test;

import java.util.Objects;

public class Student extends AbstractPayable {

	static {
		Payment.register(Student.class, (s, l) -> new Student(Objects.requireNonNull(s), Utils.requirePositive(l)));
	}

	private Student(String name, long salary) {
		super(name, salary);
	}

	/*
	 * public static Student create(String name, long salary, Payment payment) { Student st = new Student(name, salary);
	 * payment.addPayable(st); return st; }
	 */

	public long payment() {
		long halfSalary = salary / 2;
		return bonus == null ? halfSalary : halfSalary + bonus.amount;
	}

}
