package test;


public class Utils {
	static long requirePositive(long value) {
		if (value < 0) {
			throw new IllegalArgumentException("Value should be positive: " + value);
		}
		return value;
	}
}
