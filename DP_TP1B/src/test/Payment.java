package test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.function.BiFunction;

public class Payment {
	private final static HashMap<Class<? extends Payable>, BiFunction<String, Long, ? extends Payable>> factory;
	private final ArrayList<Payable> payables = new ArrayList<>();
	
	static {
		factory = new HashMap<>();
	}
	
	public static void register(Class<? extends Payable> clazz, BiFunction<String, Long, ? extends Payable> creator) {
		factory.put(Objects.requireNonNull(clazz), Objects.requireNonNull(creator));
	}

	public <T extends Payable> T addPayable(String name, long salary, Class<T> clazz) {
	  T payable = clazz.cast(factory.getOrDefault(clazz, (a, b) -> {
		  throw new IllegalStateException("Can not instanciate object of class: " + clazz.getName());
	  }).apply(Objects.requireNonNull(name), Utils.requirePositive(salary)));
	  payables.add(payable);
	  return payable;
  }

	public List<Payable> getAllPayable() {
		return Collections.unmodifiableList(payables);
	}

	public long totalPayment() {
		return payables.stream().reduce(0L, (total, em) -> total + em.payment(), (t1, t2) -> t1 + t2);
	}
}