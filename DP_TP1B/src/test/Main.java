package test;

public class Main {

	private static void loadClasses() throws ClassNotFoundException {
		ClassLoader loader = Main.class.getClassLoader();
		Class.forName("test.Employee", true, loader);
		Class.forName("test.Student", true, loader);
	}

	public static void main(String[] args) throws ClassNotFoundException {
		loadClasses();
		
		Payment payment = new Payment();
		Employee bob = payment.addPayable("bobo", 90, Employee.class);
		Payable marta = payment.addPayable("marta", 80, Employee.class);
		payment.addPayable("carol", 120, Employee.class);
		bob.setSalary(100);
		marta.setBonus(new Bonus(30));
		System.out.println(payment.getAllPayable());
		System.out.println(payment.totalPayment());

		Payable jonh = payment.addPayable("jonh", 80, Student.class);
		jonh.setBonus(new Bonus(20));
		System.out.println(payment.getAllPayable());
		System.out.println(payment.totalPayment());
	}
}
