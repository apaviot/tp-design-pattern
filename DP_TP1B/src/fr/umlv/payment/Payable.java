package fr.umlv.payment;


public interface Payable {
	long payment();
}
