package fr.umlv.payment;

import java.util.Objects;

abstract class AbstracPayable implements Payable {
	final String name;
	long salary;
	Bonus bonus;
	
	AbstracPayable(String name, long salary) {
		this.name = Objects.requireNonNull(name);
		setSalary(salary);
	}
	
	public void setSalary(long salary) {
		if(salary < 0) {
			throw new IllegalArgumentException("salary should be positive: " + salary);
		}
		this.salary = salary;
	}
	
	public void setBonus(Bonus bonus) {
		this.bonus = Objects.requireNonNull(bonus);
	}
	
	
}
