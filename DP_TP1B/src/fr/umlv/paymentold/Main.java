package fr.umlv.paymentold;

public class Main {

	public static void main(String[] args) {
		Payment payment = new Payment();
		EmployeeFactory employeeFactory = new EmployeeFactory();
		StudentFactory studentFactory  = new StudentFactory();
		Payable bob   = payment.addPayable(employeeFactory, "bob", 90);
		Payable marta = payment.addPayable(employeeFactory, "marta", 80);
		payment.addPayable(employeeFactory, "carol", 120);
		bob.setSalary(100);
		marta.setBonus(new Bonus(30));
		System.out.println(payment.getAllPayable());
		System.out.println(payment.totalPayment());

		Payable jonh = payment.addPayable(studentFactory, "jonh", 80);
		jonh.setBonus(new Bonus(20));
		System.out.println(payment.getAllPayable());
		System.out.println(payment.totalPayment());
	}
}
