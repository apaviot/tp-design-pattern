package fr.umlv.paymentold;

public class Employee extends AbstractPayable {

	Employee(String name, long salary) {
		super(name, salary);
	}

	/*
	public static Employee create(String name, long salary, Payment payment) {
		Employee em = new Employee(name, salary);
		payment.addPayable(em);
		return em;
	}
	*/

	@Override
	public	long payment() {
		return bonus == null ? salary : salary + bonus.amount;
	}
}