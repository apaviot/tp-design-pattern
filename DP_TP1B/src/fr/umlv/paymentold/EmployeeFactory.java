package fr.umlv.paymentold;

import java.util.Objects;

public class EmployeeFactory implements PayableFactory {

	@Override
	public Employee create(String name, long salary) {
		return new Employee(Objects.requireNonNull(name), Utils.requirePositive(salary));
	}


}
