package fr.umlv.paymentold;


public class StudentFactory implements PayableFactory {

	@Override
	public Payable create(String name, long salary) {
		return new Student(name, salary);
	}

}
