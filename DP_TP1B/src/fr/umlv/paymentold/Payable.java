package fr.umlv.paymentold;


public interface Payable {
	long payment();

	void setSalary(long salary);
	
	void setBonus(Bonus bonus);
}
