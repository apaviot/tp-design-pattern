package fr.umlv.paymentold;

public class Bonus {
  final long amount;

  public Bonus(long amount) {
    this.amount = Utils.requirePositive(amount);
  }
}