package fr.umlv.paymentold;

import java.util.Objects;

abstract class AbstractPayable implements Payable {
	final String name;
	long salary;
	Bonus bonus;
	
	AbstractPayable(String name, long salary) {
		this.name = Objects.requireNonNull(name);
		this.salary = Utils.requirePositive(salary);
	}
	
	public void setSalary(long salary) {
		this.salary = Utils.requirePositive(salary);
	}
	
	public void setBonus(Bonus bonus) {
		this.bonus = Objects.requireNonNull(bonus);
	}
	
	@Override
	public String toString() {
		return name;
	}

}
