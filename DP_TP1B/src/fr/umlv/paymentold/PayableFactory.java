package fr.umlv.paymentold;

public interface PayableFactory {
	Payable create(String name, long salary);
}
