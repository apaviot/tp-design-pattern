package fr.umlv.paymentold;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class Payment {
  private final ArrayList<Payable> payables = new ArrayList<>();
  
  
  public Payable addPayable(PayableFactory factory, String name, long salary) {
	  Payable p = factory.create(Objects.requireNonNull(name), Utils.requirePositive(salary));
	  payables.add(p);
	  return p;
  }

  public List<Payable> getAllPayable() {
    return Collections.unmodifiableList(payables);
  }
  
  public long totalPayment() {
	  return payables.stream().reduce(0L, (total,  em) -> total + em.payment(), (t1, t2) -> t1 + t2);
  }
}